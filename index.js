var cfenv = require("cfenv")
var appEnv = cfenv.getAppEnv()
const port = appEnv.port || 3000

var express = require("express");

var app = express();

var request = require('request');

const SapCfAxios = require('sap-cf-axios').default;

//const axios = SapCfAxios("hanaxs");

var bodyParser = require('body-parser');

app.use(bodyParser.json());

var jsonParser = bodyParser.json();

// main destination : hanaxs_cwf

//local destination : hanaxslocal

const axios = SapCfAxios("cwfdb");
// const axios = require("axios");

var url = 'CWF/BPMRules/bpmrules.xsjs';

//{"Action":"getPOSProperties","Body":"330170101"}

app.get("/bpmodata/custom/rest/BRMRulesService/getFormatPriority/1/:type", jsonParser, async (req, res) => {



    try {

        var params = '';

        if (!!req.params && !!req.params.type) {

            params = req.params.type

        }

        var obj = {

            "Action": "getFormatPriority",

            "Body": params

        };

        var url = 'BPMRules/bpmrules.xsjs';

        var data = await handleAsyncCalls(obj, url);

        res.send(data);

    } catch (error) {

        res.send(error.message);

    }



});

app.post("/bpmodata/custom/rest/CompositeRule/getCompositeDetails", jsonParser, async (req, res) => {



    try {

        var params = '';

        if (!!req.params && !!req.params.type) {

            params = req.params.type

        }

        var obj = {

            "Action": "getCompositeDetails",

            "Body": req.body

        };

        var url = 'BPMRules/bpmrules.xsjs';

        var data = await handleAsyncCalls(obj, url);

        res.send(data);

    } catch (error) {

        res.send(error.message);

    }



});

app.get("/bpmodata/custom/rest/ValidateAppointmentDateService/getAppointmentDates", jsonParser, async (req, res) => {



    console.log(req.headers);

    try {

        var params = '';

        if (!!req.params && !!req.params.type) {

            params = req.params.type

        }

        var obj = {

            "Action": "getAppointmentDates",

            "Body": params

        };

        var url = 'BPMRules/bpmrules.xsjs';

                  var data = await handleAsyncCalls(obj, url);

        // var data = ["2022-03-01", "2022-03-02", "2022-03-03", "2022-03-04", "2022-03-07", "2022-03-08", "2022-03-09", "2022-03-10", "2022-03-11", "2022-03-14", "2022-03-15", "2022-03-16", "2022-03-17", "2022-03-18", "2022-03-21", "2022-03-22", "2022-03-23", "2022-03-24", "2022-03-25", "2022-03-28", "2022-03-29"];

        res.send(JSON.stringify(data));

    } catch (error) {

        res.send(error.message);

    }



});

app.get("/bpmodata/custom/rest/BRMRulesService/getGCMPriority", jsonParser, async (req, res) => {

    try {

        var params = '';

        if (!!req.query && !!req.query.formats) {

            params = req.query.formats

        }

        var obj = {

            "Action": "getFormatPriority",

            "Body": params

        };



        var data = await handleAsyncCalls(obj, url);

        res.send(data);

    } catch (error) {

        res.send(error.message);

    }



});

app.get("/bpmodata/custom/rest/CalculateListingFeeService/getListingFee/:type", jsonParser, async (req, res) => {



    try {

        var params = '';

        if (!!req.params && !!req.params.type) {

            params = req.params.type

        }

        var obj = {

            "Action": "getListingFee",

            "Body": params

        };

        var data = await handleAsyncCalls(obj, url);

        res.send(data);

    } catch (error) {

        res.send(error.message);

    }



});

app.get("/bpmodata/custom/rest/CalculateProcessingFeeService/getProcessingFee/:type", jsonParser, async (req, res) => {



    try {

        var params = '';

        if (!!req.params && !!req.params.type) {

            params = req.params.type

        }

        var obj = {

            "Action": "getProcessingFee",

            "Body": params

        };

        var url = 'BPMRules/bpmrules.xsjs';

        var data = await handleAsyncCalls(obj, url);

        res.send(data);

    } catch (error) {

        res.send(error.message);

    }



});

app.get("/bpmodata/custom/rest/PointOfSalesService/getPOSProperties/:type", jsonParser, async (req, res) => {



    try {

        var params = '';

        if (!!req.params && !!req.params.type) {

            params = req.params.type

        }

        var obj = {

            "Action": "getPOSProperties",

            "Body": params

        };

        var url = 'BPMRules/bpmrules.xsjs';

        var data = await handleAsyncCalls(obj, url);

        res.send(data);

    } catch (error) {

        res.send(error.message);

    }

});

app.get("/bpmodata/custom/rest/BRMRulesService/getDCDefaultData/:type", jsonParser, async (req, res) => {

    var dc_details = ["DC62", "DC63", "DC64", "DC69", "DC79"]

    try {

        var params = '';

        if (!!req.params && !!req.params.type) {

            params = req.params.type

        }

        var obj = {

            "Action": "getDCDefaultData",
            "Body": dc_details.includes(params.substring(0, 4)) ? params.substring(0, 4) : ("Like *"+","+params.substring(0, 4))

        };

        console.log(obj);

        var url = 'BPMRules/bpmrules.xsjs';

        var data = await handleAsyncCalls(obj, url);
        console.log(data)
        res.send(data);

    } catch (error) {

        res.send(error.message);

    }

});

app.post("/bpmodata/custom/rest/MassExecution/getMassDefaultRules", jsonParser, async (req, res) => {



    try {

        var params = '';

        if (!!req.params && !!req.params.type) {

            params = req.params.type

        }

        var obj = {

            "Action": "getMassDefaultRules",

            "Body": req.body

        };

        var url = 'BPMRules/bpmrules.xsjs';

        var data = await handleAsyncCalls(obj, url);

        res.send(data);
        // res.send({ "dcMap": {}, "mchMap": { "11001": { "RangingDate": "" } }, "mchSubCatMap": { "110010101": "true,false" } });

    } catch (error) {

        res.send(error.message);

    }



});

app.post("/bpmodata/custom/rest/MassExecution/getMassDefaultRules1", jsonParser, async (req, res) => {



    try {

        var params = '';

        if (!!req.params && !!req.params.type) {

            params = req.params.type

        }

        var obj = {

            "Action": "getMassDefaultRules",

            "Body": req.body

        };

        var url = 'BPMRules/bpmrules.xsjs';

        var data = await handleAsyncCalls(obj, url);

        // res.send(data);
        res.send({ "dcMap": {}, "mchMap": { "11001": { "RangingDate": "" } }, "mchSubCatMap": { "110010101": "true,false" } });

    } catch (error) {

        res.send(error.message);

    }



});

const handleAsyncCalls = async function (obj, url) {

    // console.log(JSON.stringify(obj));



    const response = await axios({

        method: 'POST',

        // url: url,
        url: "CWF/BPMRules/bpmrules.xsjs",
        // url: "https://p7yygkeazazl0in1-cwf-xsjs.cfapps.ap11.hana.ondemand.com/CWF/BPMRules/bpmrules.xsjs",
        // url: "https://btp-cf-fp-dev-new-dev-cwf-xsjs.cfapps.ap11.hana.ondemand.com/CWF/BPMRules/bpmrules.xsjs",

        params: {

            "$format": 'json'

        },

        headers: {

            "content-type": "application/json"

        },

        data: JSON.stringify(obj)

    });

    if (!!response && !!response.data) {

        return response.data;

    }



};

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})

